package repository;

import bean.Aliment;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

public interface AlimentRepo extends MongoRepository<Aliment,String> {
    @Query(value = "{ 'denumire' : ?0 }")
    Aliment findAlimentByName(String denumire);
}
