package com.aliment.demo;

import bean.Aliment;
import configuration.AppConfing;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;
import service.AlimentService;

import java.util.List;

@SpringBootApplication
public class AppApplication {

    public static void main(String[] args) {

        AbstractApplicationContext context = new AnnotationConfigApplicationContext(AppConfing.class);
        AlimentService alimentService =(AlimentService) context.getBean("alimentService");

        alimentService.deleteAll();

        Aliment mar = new Aliment("mar",100,80);
        alimentService.createAliment(mar);

        Aliment pepene = new Aliment("pepene",100,60);
        alimentService.createAliment(pepene);

        Aliment ciocolata = new Aliment("ciocolata",100,560);
        alimentService.createAliment(ciocolata);

        Aliment carnePorc = new Aliment("carne de porc",100,110);
        alimentService.createAliment(carnePorc);

        Aliment banana = new Aliment("banana",100,85);
        alimentService.createAliment(banana);

        System.out.println("Find all:");
        List<Aliment> all = alimentService.findAll();
        for(Aliment al : all){
            System.out.println(al);
        }
        System.out.println("Find by nume = pepene: " + alimentService.findAlimentByName("pepene"));

        System.out.println("delete mar: " );
        alimentService.deleteAliment(mar);

        System.out.println("update ciocolata");
        ciocolata.setDenumire("ciocolata neagra");
        alimentService.updateAliment(ciocolata);


        System.out.println("Find all:");
        List<Aliment> all2 = alimentService.findAll();
        for(Aliment al : all2){
            System.out.println(al);
        }
    }

}
