package service;

import bean.Aliment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import repository.AlimentRepo;

import java.util.List;

@Service("alimentService")
public class AlimentServiceImpl implements AlimentService {
    @Autowired
    AlimentRepo alimentRepo;

    @Override
    public void createAliment(Aliment al) {
        Aliment aliment =  alimentRepo.insert(al);
    }

    @Override
    public void updateAliment(Aliment al) {
        Aliment aliment = alimentRepo.save(al);
    }

    @Override
    public void deleteAliment(Aliment al) {
        alimentRepo.delete(al);
    }

    @Override
    public void deleteAll() {
        alimentRepo.deleteAll();
    }

    @Override
    public Aliment findAlimentByName(String denumire) {
        return alimentRepo.findAlimentByName(denumire);
    }

    @Override
    public List<Aliment> findAll() {
        return alimentRepo.findAll();
    }
}
